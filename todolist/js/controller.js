let layThongTinTuForm = () => {
  var jobContent = document.getElementById("newTask").value;

  let task = {
    name: jobContent,
  };
  return task;
};

// render ra dao diện khi ấn thêm
let renderTask = (listTask) => {
  let contentHtml = "";
  listTask.forEach((item) => {
    contentHtml += `<ul>
                                <li>
                                <p>${item.name}</p>
                                <div>
                                <span class="cursor" style="font-size:18px" onclick="deleteTask('${item.name}')"><i class="fa fa-trash"></i></span>
                                <span class="cursor" style="font-size:20px" onclick="workDon('${item.name}')"><i class="fa fa-check-circle"></i></span>
                                </div>
                                </li>
                            </ul>`;
  });
  document.querySelector("#todo").innerHTML = contentHtml;
};

// render việc đã làm ra dao diện khi ấn nút phía sau thùng xóa.
let renderTaskDon = (listDaLam) => {
  let contentHtml = "";
  listDaLam.forEach((item) => {
    contentHtml += `<ul>
                                <li>
                                <p>${item.name}</p>
                                <div>
                                <span class="cursor" style="font-size:18px" onclick="deleteTaskDon('${item.name}')"><i class="fa fa-trash"></i></span>
                                <span style="color:green;font-size:20px;"><i class="fa fa-check-circle"></i></span>
                                </div>
                                </li>
                            </ul>`;
  });
  document.querySelector("#completed").innerHTML = contentHtml;
};
