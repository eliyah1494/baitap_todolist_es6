var tasks = [];
// get data Task from local Storage
var dataTaskJson = localStorage.getItem("TASKS");
if (dataTaskJson) {
  tasks = JSON.parse(dataTaskJson);
  renderTask(tasks);
}

var taskDon = [];
// get data Complete mission from local Storage
var dataTaskDonJson = localStorage.getItem("TASKS_DON");
if (dataTaskDonJson) {
  taskDon = JSON.parse(dataTaskDonJson);
  renderTaskDon(taskDon);
}

var btnAdd = document.querySelector("#addItem");
btnAdd.addEventListener("click", function () {
  let task = layThongTinTuForm();
  if (!task.name) {
    alert("Vui lòng nhập tên công việc");
    return;
  }

  tasks.push(task);

  // save work tasks to local storage
  var taskJson = JSON.stringify(tasks);
  localStorage.setItem("TASKS", taskJson);
  renderTask(tasks);
  //   reset input
  document.getElementById("newTask").value = "";
});

// function delete Task
let deleteTask = (name) => {
  var index;
  for (let i = 0; i < tasks.length; i++) {
    if (tasks[i].name === name) {
      index = i;
    }
  }
  tasks.splice(index, 1);
  // save work tasks to local storage
  var taskJson = JSON.stringify(tasks);
  localStorage.setItem("TASKS", taskJson);
  renderTask(tasks);
};

// function get Complete mission
let workDon = (name) => {
  var index;
  for (let i = 0; i < tasks.length; i++) {
    if (tasks[i].name === name) {
      index = i;
    }
  }
  taskDon.push(tasks[index]);

  // save Task Completion to local storage
  var taskDonJson = JSON.stringify(taskDon);
  localStorage.setItem("TASKS_DON", taskDonJson);

  tasks.splice(index, 1);
  // save work tasks to local storage
  var taskJson = JSON.stringify(tasks);
  localStorage.setItem("TASKS", taskJson);

  renderTask(tasks);
  renderTaskDon(taskDon);
};

// function delete Complete mission
let deleteTaskDon = (name) => {
  var index;
  for (let i = 0; i < taskDon.length; i++) {
    if (taskDon[i].name === name) {
      index = i;
    }
  }
  taskDon.splice(index, 1);
  // save Task Completion to local storage
  var taskDonJson = JSON.stringify(taskDon);
  localStorage.setItem("TASKS_DON", taskDonJson);
  renderTaskDon(taskDon);
};

document.getElementById("two").addEventListener("click", function () {
  // Sắp xếp
  tasks.sort(function (a, b) {
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return -1;
    }
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
      return 1;
    }
    return 0;
  });
  renderTask(tasks);
});
document.getElementById("three").addEventListener("click", function () {
  // Sắp xếp
  tasks.sort(function (a, b) {
    if (a.name.toLowerCase() > b.name.toLowerCase()) {
      return -1;
    }
    if (a.name.toLowerCase() < b.name.toLowerCase()) {
      return 1;
    }
    return 0;
  });
  renderTask(tasks);
});

// trở lại ban đầu khi load trang
document.getElementById("all").addEventListener("click", function () {
  location.reload();
});
